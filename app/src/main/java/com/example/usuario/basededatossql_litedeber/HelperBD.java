package com.example.usuario.basededatossql_litedeber;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HelperBD extends SQLiteOpenHelper {

    public HelperBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table estudiante(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre text, materia text," +
                " nota1 text, nota2 text, promedio text, estado text);");
    }
    public void insertar(String nombre, String materia, String nota1, String nota2, String promedio, String estado){
        ContentValues values = new ContentValues();
        values.put("nombre",nombre);
        values.put("materia",materia);
        values.put("nota1",nota1);
        values.put("nota2", nota2);
        values.put("promedio", promedio);
        values.put("estado", estado);
        this.getWritableDatabase().insert("estudiante", null,values);
    }

    public String leer(){
        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM estudiante", null);
        if (cursor.moveToFirst()){
            do{
                String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String materia = cursor.getString(cursor.getColumnIndex("materia"));
                String nota1 = cursor.getString(cursor.getColumnIndex("nota1"));
                String nota2 = cursor.getString(cursor.getColumnIndex("nota2"));
                String promedio = cursor.getString(cursor.getColumnIndex("promedio"));
                String estado = cursor.getString(cursor.getColumnIndex("estado"));

                consulta += "****************************"+"\n\nEL ESTUDIANTE: "+nombre
                        +"\nEN LA MATERIA DE "+ materia
                        +"\nTIENE EN EL PRIMER PARCIAL: " + nota1
                        +"\nY EN EL SEGUNDO PARCIAL: " +nota2
                        +"\nLO QUE NOS DA UN PROMEDIO DE: " +promedio
                        +"\nENTONCES EL ESTUDIANTE: " + estado +"\n\n\n****************************";
            }while (cursor.moveToNext());
        }
        return consulta;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
