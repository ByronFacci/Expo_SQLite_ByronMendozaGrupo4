package com.example.usuario.basededatossql_litedeber;

import android.app.Dialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Interfaz_BD extends AppCompatActivity implements View.OnClickListener {

    Button botonAgregar, botonListar;
    EditText CajaNombres, Cajamaterias, CajaNota1, CajaNota2;
    TextView datos;
    HelperBD estudiantedb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interfaz__bd);

        CajaNombres = findViewById(R.id.TXTNombre);
        Cajamaterias = findViewById(R.id.TXTMateria);
        CajaNota1 = findViewById(R.id.TXTNota1);
        CajaNota2 = findViewById(R.id.TXTNota2);
        datos = findViewById(R.id.lblDatos);

        botonAgregar = findViewById(R.id.BTNAgregar);
        botonListar = findViewById(R.id.BTNListar);

        botonAgregar.setOnClickListener(this);
        botonListar.setOnClickListener(this);

        estudiantedb = new HelperBD(this, "bda", null, 1);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.BTNAgregar:
                if (CajaNota2.getText().toString().equals("")||CajaNota1.getText().toString().equals("")||
                        Cajamaterias.getText().toString().equals("")||CajaNombres.getText().toString().equals("")){

                }else {
                    Double Nota1 = Double.valueOf(CajaNota1.getText().toString());
                    Double Nota2 = Double.valueOf(CajaNota2.getText().toString());
                    Double Promedio = (Nota1 + Nota2) / 2;

                    if (Promedio<5){
                        estudiantedb.insertar(CajaNombres.getText().toString(), Cajamaterias.getText().toString(), CajaNota1.getText().toString(),
                                CajaNota2.getText().toString(), Promedio.toString(), "REPROBADO");
                        CajaNombres.setText("");
                        Cajamaterias.setText("");
                        CajaNota2.setText("");
                        CajaNota1.setText("");
                        CajaNombres.requestFocus();
                    }else if (Promedio<7){
                        estudiantedb.insertar(CajaNombres.getText().toString(), Cajamaterias.getText().toString(), CajaNota1.getText().toString(),
                                CajaNota2.getText().toString(), Promedio.toString(), "SUPLETORIO");
                        CajaNombres.setText("");
                        Cajamaterias.setText("");
                        CajaNota2.setText("");
                        CajaNota1.setText("");
                        CajaNombres.requestFocus();
                    }else if (Promedio>=7){
                        estudiantedb.insertar(CajaNombres.getText().toString(), Cajamaterias.getText().toString(), CajaNota1.getText().toString(),
                                CajaNota2.getText().toString(), Promedio.toString(),  "APROBADO");
                        CajaNombres.setText("");
                        Cajamaterias.setText("");
                        CajaNota2.setText("");
                        CajaNota1.setText("");
                        CajaNombres.requestFocus();
                    }
                }
                break;

            case R.id.BTNListar:

                datos.setText("");
                datos.append(estudiantedb.leer());
                break;
        }
    }

}

